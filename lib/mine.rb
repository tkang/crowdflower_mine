require "crowdflower_mine/version"

class Mine
  attr_accessor :x, :y, :radius

  def initialize(x, y, radius)
    self.x = x.to_i
    self.y = y.to_i
    self.radius = radius.to_i
  end

  def triggers_blast?(another_mine)
    return true if self.x == another_mine.x and self.y == another_mine.y
    return false if self.radius <= 0
    sum_of_squares = (another_mine.x - self.x) ** 2 + (another_mine.y - self.y) ** 2
    Math.sqrt(sum_of_squares) <= self.radius
  end

  def to_s
    "#{self.x} #{self.y} #{self.radius}"
  end
end
