require "crowdflower_mine/version"

module CrowdflowerMine
  def self.find_max_blast_counts_from_file(filename)
    find_max_blast_counts(load_mine_data(filename))
  end

  def self.find_max_blast_counts(unblasted_mines)
    return { max_blast_count: 0, max_blasting_mine: 'N/A' } if unblasted_mines.nil? or unblasted_mines.empty?
    blasted_mines_starting_at = unblasted_mines.map { |i| get_blasted_mines_at_times(i, unblasted_mines) }
    blast_counts = blasted_mines_starting_at.map { |blasted_list| count_blasted(blasted_list) }

    max_blast_count = blast_counts.max
    idx = blast_counts.rindex(max_blast_count)
    max_blasting_mine = unblasted_mines[idx]
    { max_blast_count: max_blast_count, max_blasting_mine: max_blasting_mine }
  end

  private

  def self.count_blasted(l)
    l.map { |i| i.size }.inject(:+)
  end

  # returns blasted mines at times
  def self.get_blasted_mines_at_times(first_mine, unblasted_mines)
    blasted_mines = []

    # at time 0
    time = 0
    blasted_mines[time] = [ first_mine ]
    unblasted_mines -= [ first_mine ]
    blasted = true

    while blasted
      # at time n
      time += 1
      tmp = []
      unblasted_mines.each do |unblasted_mine|
        blasted_mines[time - 1].each do |bm|
          tmp << unblasted_mine if bm.triggers_blast?(unblasted_mine)
        end
      end
      tmp = tmp.compact.uniq
      blasted = !tmp.empty?
      blasted_mines[time] = tmp
      unblasted_mines -= tmp
    end

    blasted_mines
  end

  def self.load_mine_data(inputfile)
    File.readlines(inputfile).map { |line| Mine.new(*line.strip.split(" ")) }
  end

end
