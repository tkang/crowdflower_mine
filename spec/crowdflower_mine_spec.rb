require 'spec_helper'

describe CrowdflowerMine, "find_max_blast_counts" do
  context "for empty input" do
    it do
      h = CrowdflowerMine.find_max_blast_counts([])
      h.should == { max_blast_count: 0, max_blasting_mine: 'N/A' }
    end
  end

  context "for a single input" do
    let(:mine) { Mine.new(1,1,1) }
    it do
      h = CrowdflowerMine.find_max_blast_counts([mine])
      h.should == { max_blast_count: 1, max_blasting_mine: mine }
    end
  end

  context "for mines that do not blast one another" do
    let(:mine) { Mine.new(1,1,1) }
    let(:mine_2) { Mine.new(100,100,1) }
    let(:mine_3) { Mine.new(1000,1000,1) }

    it "should return the last one as max_blasting_mine" do
      h = CrowdflowerMine.find_max_blast_counts([mine, mine_2, mine_3])
      h.should == { max_blast_count: 1, max_blasting_mine: mine_3 }
    end
  end

  context "for mines that blast one after another" do
    let(:mine) { Mine.new(0,0,10) }
    let(:mine_2) { Mine.new(5,5,10) }
    let(:mine_3) { Mine.new(10,10,10) }

    it "should return the last one as max_blasting_mine" do
      h = CrowdflowerMine.find_max_blast_counts([mine, mine_2, mine_3])
      h.should == { max_blast_count: 3, max_blasting_mine: mine_3 }
    end
  end

  context "if a mine out of 3 is far away" do
    let(:mine) { Mine.new(0,0,10) }
    let(:mine_2) { Mine.new(5,5,10) }
    let(:mine_3) { Mine.new(10000,10000,10) }

    it "max_blast_count should be 2" do
      h = CrowdflowerMine.find_max_blast_counts([mine, mine_2, mine_3])
      h.should == { max_blast_count: 2, max_blasting_mine: mine_2 }
    end
  end

  context "if there are multiple groups of mines blast ones nearby" do
    let(:mine) { Mine.new(0,0,10) }
    let(:mine_2) { Mine.new(1,1,10) }
    let(:mine_3) { Mine.new(2,2,10) }
    let(:mine_4) { Mine.new(10000,10000,10) }
    let(:mine_5) { Mine.new(10001,10001,10) }

    it "max_blast_count should be 3" do
      h = CrowdflowerMine.find_max_blast_counts([mine, mine_2, mine_3, mine_4, mine_5])
      h.should == { max_blast_count: 3, max_blasting_mine: mine_3 }
    end
  end
end
