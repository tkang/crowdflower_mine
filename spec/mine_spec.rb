require 'spec_helper'

describe Mine, "triggers_blast?" do
  let(:mine) { Mine.new(0, 0, 2) }
  let(:mine_with_zero_radius) { Mine.new(0, 0, 0) }

  it "should blast a mine on same coordinate" do
    mine_with_same_coordinate = Mine.new(mine.x, mine.y, 1)
    mine.triggers_blast?(mine_with_same_coordinate).should == true
  end

  it "mine with zero radius should not blast another mine" do
    another_mine = Mine.new(2, 3, 1)
    mine_with_zero_radius.triggers_blast?(another_mine).should == false
  end

  it "should trigger a mine within its blast radius" do
    mine_within_radius = Mine.new(1, 1, 1)
    mine.triggers_blast?(mine_within_radius).should == true
  end

  it "should trigger a mine at its blast radius" do
    mine_at_radius = Mine.new(0, 2, 1)
    mine.triggers_blast?(mine_at_radius).should == true
  end

  it "should not trigger a mine outside its blast radius" do
    mine_outside_radius = Mine.new(0, 3, 1)
    mine.triggers_blast?(mine_outside_radius).should == false
  end
end
